* OS Installation Scripts
This folder is for installing and configuring a minimal installation of an OS. For example, perform a clean install of kali with no tools selected, then run ~kali-install.sh~ to install and configure the OS.
