#!/bin/bash

# Script for setting up a reverse SSH shell
# Creates a portforward on a publicly reachable device to be used as
# a jump box. Removes the need to open SSH to teh internet to access
# a device behind a firwall.

# Version: 2.0

# set colors for use throughout the script.
RED="\033[01;31m"
GREEN="\033[01;32m"
YELLOW="\033[01;33m"
BLUE="\033[01;34m"
BOLD="\033[01;01m"
RESET="\033[00m"
export DEBIAN_FRONTEND=noninteractive

# check if root, if not, exit script.
if (( $EUID != 0 ))
then
    echo "[${RED}!${RESET}] you are ${RED}not${RESET} root"
    exit
fi

# directory to put the script for starting the SSH tunnel
installdir=/opt

# random high port for remote portforwarding
PORT=$(shuf -i 40000-60000 -n 1)

# name of the new ssh key
KEYPATH="$HOME/.ssh/id_ed25519_$(hostname)"

# temp key name. change this to your private key name if you manualy copy the private key
TMPKEY="$HOME/.ssh/id_ed25519_server"

# IP of jump box (publicly reachable device)
pubIP=""

# user for key installation
pubUser=""

# make sure ssh and cron are installed, and enable ssh
echo -e "[${GREEN}+${RESET}] setting up SSH reverse shell on port ${YELLOW}${PORT}${RESET}"
apt-get -qq update && \
    apt-get -qq install -y ssh openssh-server cron && \
    systemctl enable ssh

# create the private key used to access the publicly reachable device and install the new
# ssh key in authorized_keys. comment this out if you manually copy the private key
echo -e "[${GREEN}+${RESET}] ${YELLOW}Creating a temp ssh key for this installation${RESET}"
mkdir -p "$HOME/.ssh/" && \
    touch "$TMPKEY" && \
    chmod 600 "$TMPKEY" && \
    cat <<EOF > "$TMPKEY"
-----BEGIN OPENSSH PRIVATE KEY-----
place your private key text here
-----END OPENSSH PRIVATE KEY-----
EOF

# create the new keypair
echo -e "[${GREEN}+${RESET}] ${YELLOW}Creating an ssh key for this installation${RESET}"
ssh-keygen -f "$KEYPATH" -t ed25519 -N "" -C "${PORT}_$(hostname)_$(date +%d-%b-%Y_%H:%M)" && \
    chown $USER:$USER "$KEYPATH" && \
    chown $USER:$USER "$KEYPATH.pub"

# add key options for installation to authorized_keys on the publicly reachable device
AUTHKEY="command=\"exit\",restrict,port-forwarding $(cat $KEYPATH.pub)"

# install the new key on the publicly reachable device
echo -e "[${GREEN}+${RESET}] ${YELLOW}installing key on ${RED}${pubIP}${RESET}"
ssh $pubUser@$pubIP -i "$TMPKEY" -t "echo $AUTHKEY"

# shred the installation private key
echo -e "[${GREEN}+${RESET}] ${YELLOW}removing the temp key${RESET}"
shred -u "$TMPKEY"

# create the SSH reverse shell script
echo -e "[${GREEN}+${RESET}] making SSH reverse shell script"
cd $installdir && \
    cat <<EOF > $installdir/ssh-backup.sh
#!/bin/bash
if [ "\$(ss -t | grep -oh $pubIP | sort -u)" != "$pubIP" ]; then
   ssh -Nf -R$PORT:127.0.0.1:22 -i $HOME/.ssh/id_ed25519_$(hostname) $pubUser@$pubIP
fi
EOF

# create a cron job to keep the SSH reverse shell alive if it dies or the device is rebooted
echo -e "[${GREEN}+${RESET}] installing cron job"
chmod +x $installdir/ssh-backup.sh && \
    echo "@reboot /opt/ssh-backup.sh" >> /var/spool/cron/crontabs/root && \
    echo "*/1 * * * * /opt/ssh-backup.sh" >> /var/spool/cron/crontabs/root

# prevent SSH from asking about hosts
echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config && \
    echo "UserKnownHostsFile /dev/null" >> /etc/ssh/ssh_config

# restart SSH
systemctl restart ssh.service
